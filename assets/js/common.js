(function ($) { // Begin jQuery
  $(function () { // DOM ready
    // If a link has a dropdown, add sub menu toggle.
    $('nav ul li a:not(:only-child)').click(function (e) {
      $(this).siblings('.nav-dropdown').toggle();
      // Close one dropdown when selecting another
      $('.nav-dropdown').not($(this).siblings()).hide();
      e.stopPropagation();
    });
    // Clicking away from dropdown will remove the dropdown class
    $('html').click(function () {
      $('.nav-dropdown').hide();
    });
    // Toggle open and close nav styles on click
    $('#nav-toggle').click(function () {
      $('nav ul').slideToggle();
    });
    // Hamburger to X toggle
    $('#nav-toggle').on('click', function () {
      this.classList.toggle('active');
    });
    /* */
    const parent = document.querySelector('.range-slider');

    if (!parent) {
      return;
    }

    const rangeS = parent.querySelectorAll('input[type="range"]'),
      numberS = parent.querySelectorAll('input[type="number"]');

    rangeS.forEach((el) => {
      el.oninput = () => {
        let slide1 = parseFloat(rangeS[0].value),
          slide2 = parseFloat(rangeS[1].value);

        if (slide1 > slide2) {
          [slide1, slide2] = [slide2, slide1];
        }

        numberS[0].value = slide1;
        numberS[1].value = slide2;
      }
    });

    numberS.forEach((el) => {
      el.oninput = () => {
        let number1 = parseFloat(numberS[0].value),
          number2 = parseFloat(numberS[1].value);

        if (number1 > number2) {
          let tmp = number1;
          numberS[0].value = number2;
          numberS[1].value = tmp;
        }

        rangeS[0].value = number1;
        rangeS[1].value = number2;
      }
    });
    /** */

  }); // end DOM ready

  $(document).ready(function () {
    /* Search */
    $(document).on("click", ".block-search > .block-title", function (e) {
      e.preventDefault();
      $(this).parent().toggleClass('active');
      $('header.page-header').toggleClass('ative-search');
      if ($('.search-icon-popup').length) {
        $('body').toggleClass('search-popup');
      }
    });

    $(document).on("click", ".page-header .search-icon-left .block-title.theme-header-icon", function (e) {
      e.preventDefault();
      disableBodyScroll();
      $(this).parent().toggleClass('show-search-box');
    });
    $('.example').beefup();

    var quantitiy = 0;
    $('.quantity-right-plus').click(function (e) {

      // Stop acting like a button
      e.preventDefault();
      // Get the field name
      var quantity = parseInt($('#quantity').val());

      // If is not undefined

      $('#quantity').val(quantity + 1);


      // Increment

    });

    $('.quantity-left-minus').click(function (e) {
      // Stop acting like a button
      e.preventDefault();
      // Get the field name
      var quantity = parseInt($('#quantity').val());

      // If is not undefined

      // Increment
      if (quantity > 0) {
        $('#quantity').val(quantity - 1);
      }
    });
    //footer for mobile
    var alterClass = function() {
      var ww = document.body.clientWidth;
      if (ww < 798) {
        $(".footerBlockCols").addClass("beefup example");
        $(".footer_block_title").addClass("beefup__head");
        $(".footer_list_item").addClass("beefup__body");
        $('.example').beefup();
        
        
      } else if (ww >= 800) {
        $(".footerBlockCols").removeClass("beefup example");
        $(".footer_block_title").removeClass("beefup__head");
        $(".footer_list_item").removeClass("beefup__body");
      };
    };
    $(window).resize(function(){
      alterClass();
    });
    //Fire it when the page first loads:
    alterClass();
    
    //load more
    $(".product-item").slice(0, 6).show();
    $("#loadMore").on("click", function(e){
      e.preventDefault();
      $(".product-item:hidden").slice(0, 6).slideDown();
      if($(".product-item:hidden").length == 0) {
        $("#loadMore").text("No Content").addClass("noContent");
      }
    });

  })
  /* */


    
  /* */
})(jQuery); // end jQuery